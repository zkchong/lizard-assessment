function CategoryFilter({ categories, selectedCategories, onCategorySelect }) {
  return (
    <div className="bg-white rounded-md shadow p-4 border-2">
      <h2 className="text-gray-800 font-medium mb-2">Filter by Category</h2>
      <form className="space-y-2">
        {categories.map((category) => (
          <label
            key={category}
            className="inline-flex items-center space-x-2 mx-2 cursor-pointer hover:font-bold transform hover:scale-105 transition duration-300"
          >
            <input
              className="form-checkbox text-blue-600 h-4 w-4 cursor-pointer"
              type="checkbox"
              name={category}
              checked={selectedCategories.includes(category)}
              onChange={() => onCategorySelect(category)}
            />
            <span className="text-gray-700">{category}</span>
          </label>
        ))}
      </form>
    </div>
  );
}
export default CategoryFilter;
