import { screen, fireEvent, render } from '@testing-library/react';
import CategoryFilter from '../CategoryFilter';

describe('CategoryFilter component', () => {
  const categories = ['Category 1', 'Category 2', 'Category 3'];
  const selectedCategories = ['Category 3'];
  const onCategorySelect = jest.fn();

  it('should match snapshot', () => {
    expect(
      render(
        <CategoryFilter
          categories={categories}
          selectedCategories={selectedCategories}
          onCategorySelect={onCategorySelect}
        />
      )
    ).toMatchSnapshot();
  });

  it('renders the filter title', () => {
    render(
      <CategoryFilter
        categories={categories}
        selectedCategories={selectedCategories}
        onCategorySelect={onCategorySelect}
      />
    );
    const title = screen.getByText('Filter by Category');
    expect(title).toBeInTheDocument();
  });

  it('renders all category checkboxes', () => {
    render(
      <CategoryFilter
        categories={categories}
        selectedCategories={selectedCategories}
        onCategorySelect={onCategorySelect}
      />
    );
    const checkboxes = screen.getAllByRole('checkbox');
    expect(checkboxes.length).toBe(categories.length);

    categories.forEach((category, index) => {
      const checkbox = checkboxes[index];
      expect(checkbox).toHaveAttribute('name', category);
      expect(checkbox.checked).toBe(selectedCategories.includes(category));

      const label = screen.getByText(category);
      expect(label).toBeInTheDocument();
    });
  });

  it('calls onCategorySelect when a checkbox is clicked', () => {
    render(
      <CategoryFilter
        categories={categories}
        selectedCategories={selectedCategories}
        onCategorySelect={onCategorySelect}
      />
    );
    const checkbox = screen.getByRole('checkbox', { name: 'Category 1' });
    fireEvent.click(checkbox);
    expect(onCategorySelect).toHaveBeenCalledWith('Category 1');
  });
});
