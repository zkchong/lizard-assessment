import { createBrowserRouter } from 'react-router-dom';
import ListingPage from './pages/listing';
import DetailPage from './pages/details';

export const routes = createBrowserRouter([
  {
    path: '/',
    element: <ListingPage />,
  },
  {
    path: '/details',
    element: <DetailPage />,
  },
]);
