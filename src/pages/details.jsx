import { useEffect } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

const DetailPage = () => {
  const location = useLocation();
  const navigate = useNavigate();

  const post = location.state?.post;
  useEffect(() => {
    if (!post) {
      navigate('/');
    }
  }, [post, navigate]);

  return (
    <div className="max-w-4xl mx-auto my-4 ">
      <h1 className="text-3xl font-bold text-gray-800 mb-4 capitalize">
        {post?.title ?? ''}
      </h1>
      <div className="flex items-center mb-4">
        <p className="text-gray-700">{post?.author?.name ?? ''}</p>
      </div>
      <p className="text-gray-700 mb-4">LOREM IPSUM</p>
      <div className="flex flex-wrap mb-4">
        {post?.categories.map((category) => (
          <div
            className="bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2"
            key={category.id}
          >
            {category.name}
          </div>
        ))}
      </div>
      <p className="text-gray-700">Actual content here please</p>
    </div>
  );
};

export default DetailPage;
