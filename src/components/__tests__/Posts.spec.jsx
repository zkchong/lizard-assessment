import { screen, fireEvent, render } from '@testing-library/react';
import Posts from '../Posts';

describe('Posts component', () => {
  const posts = [
    {
      id: 1,
      title: 'First Post',
      summary: 'Lorem ipsum dolor sit amet',
      author: { name: 'John Doe' },
      publishDate: '2022-01-01',
      categories: [{ name: 'Category A' }, { name: 'Category B' }],
    },
    {
      id: 2,
      title: 'Second Post',
      summary: 'Consectetur adipiscing elit',
      author: { name: 'Jane Smith' },
      publishDate: '2022-01-02',
      categories: [{ name: 'Category B' }, { name: 'Category C' }],
    },
  ];

  const handleOnClick = jest.fn();

  it('should match snapshot', () => {
    expect(
      render(<Posts posts={posts} handleOnClick={handleOnClick} />)
    ).toMatchSnapshot();
  });

  it('renders a list of posts', () => {
    render(<Posts posts={posts} handleOnClick={handleOnClick} />);
    const postElements = screen.getAllByRole('listitem');
    expect(postElements.length).toBe(posts.length);

    posts.forEach((post, index) => {
      const postElement = postElements[index];
      expect(postElement).toHaveTextContent(post.title);
      expect(postElement).toHaveTextContent(post.summary);
      expect(postElement).toHaveTextContent(post.author.name);
      expect(postElement).toHaveTextContent(
        new Date(post.publishDate).toLocaleDateString()
      );
      expect(postElement).toHaveTextContent(
        post.categories.map((category) => category.name).join(', ')
      );
    });
  });

  it('calls handleOnClick when a post is clicked', () => {
    render(<Posts posts={posts} handleOnClick={handleOnClick} />);
    const postElement = screen.getByText(posts[0].title);
    fireEvent.click(postElement);
    expect(handleOnClick).toHaveBeenCalledWith(posts[0]);
  });
});
