module.exports = {
  verbose: true,
  clearMocks: true,
  testEnvironment: 'jsdom',
  roots: ['<rootDir>/src/'],
  transform: {
    '^.+\\.(t|j)sx?$': '@swc/jest',
  },
  setupFilesAfterEnv: ['<rootDir>/src/setupTests.js'],
};
