import { useEffect, useState, useMemo } from 'react';
import { useNavigate, useSearchParams } from 'react-router-dom';

import { getPosts } from '../api/posts';
import CategoryFilter from '../components/CategoryFilter';
import Posts from '../components/Posts';
import usePagination from '../hooks/usePagination';
import LoadMoreButton from '../components/LoadMoreButton';

function sanitizePosts(posts) {
  let uniqueCategories = {};
  const sanitizedPosts = [];

  for (const post of posts) {
    const categories = [];
    for (const category of post.categories) {
      if (!uniqueCategories[category.name]) {
        categories.push(category);
        uniqueCategories[category.name] = true;
      }
    }
    uniqueCategories = {};
    sanitizedPosts.push({ ...post, categories });
  }

  return sanitizedPosts;
}

function ListingPage() {
  const navigate = useNavigate();
  const [, setSearchParams] = useSearchParams();

  const [posts, setPosts] = useState([]);
  const [selectedCategories, setSelectedCategories] = useState([]);

  useEffect(() => {
    getPosts().then(({ data }) => {
      setPosts(sanitizePosts(data.posts));
    });
  }, []);

  useEffect(() => {
    if (selectedCategories.length) {
      const searchParams = new URLSearchParams();
      searchParams.set('categories', encodeURI(selectedCategories.join(',')));
      setSearchParams(searchParams);
    } else {
      setSearchParams(new URLSearchParams());
    }
  }, [selectedCategories, setSearchParams]);

  function handleCategorySelect(categoryName) {
    if (selectedCategories.includes(categoryName)) {
      // remove selected category
      setSelectedCategories(
        selectedCategories.filter((category) => category !== categoryName)
      );
    } else {
      setSelectedCategories([...selectedCategories, categoryName]);
    }
  }

  const filteredPosts = useMemo(
    () =>
      selectedCategories.length > 0
        ? posts.filter((post) =>
            post.categories.some((category) =>
              selectedCategories.includes(category.name)
            )
          )
        : posts,
    [posts, selectedCategories]
  );

  const flattenedCategories = useMemo(() => {
    // Create an array of all category objects
    const allCategories = posts.flatMap((post) => post.categories);

    // Map each category object to an object with id and name properties
    const categoriesNames = allCategories.map((category) => category.name);

    // Remove duplicates
    const uniqueCategories = [...new Set(categoriesNames)];

    return uniqueCategories;
  }, [posts]);

  const handlePostClick = (post) => {
    navigate('/details', {
      state: {
        post,
      },
    });
  };

  const {
    currentArray: currentPostsArray,
    loadMore,
    hasMore,
  } = usePagination(filteredPosts, 5);

  return (
    <div className="h-screen">
      <h1 className="text-4xl font-bold text-gray-800 text-center border-b-2 py-2">
        Lizard Blog Posts
      </h1>
      <div className="h-full">
        <CategoryFilter
          categories={flattenedCategories}
          selectedCategories={selectedCategories}
          setSelectedCategories={setSelectedCategories}
          onCategorySelect={handleCategorySelect}
        />
        <Posts posts={currentPostsArray} handleOnClick={handlePostClick} />
        {!!hasMore && <LoadMoreButton loadMore={loadMore} />}
      </div>
    </div>
  );
}

export default ListingPage;
