import { useState } from 'react';

function usePagination(arr, itemsPerPage) {
  const [currentPage, setCurrentPage] = useState(1);
  const [previousArr, setPreviousArr] = useState([]);

  const totalPages = Math.ceil(arr.length / itemsPerPage);

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const currentArray = [...previousArr, ...arr.slice(startIndex, endIndex)];

  const loadMore = () => {
    setPreviousArr(currentArray);
    setCurrentPage((prevPage) => prevPage + 1);
  };

  const hasMore = currentPage < totalPages;

  return {
    currentArray,
    loadMore,
    hasMore,
  };
}

export default usePagination;
