function Posts({ posts, handleOnClick }) {
  return (
    <ul className="space-y-2 cursor-pointer">
      {posts.map((post) => (
        <li
          key={post.id}
          className="bg-white rounded-md shadow p-6 hover:shadow-md"
          onClick={() => handleOnClick(post)}
        >
          <h2 className="text-gray-800 font-medium mb-2 capitalize">
            {post.title}
          </h2>
          <p className="text-gray-400 mb-2">
            By {post.author.name} on{' '}
            {new Date(post.publishDate).toLocaleDateString()}
          </p>
          <p className="text-gray-600 mb-4">{post.summary}</p>
          <p className="space-x-2 text-black">
            Categories:{' '}
            {post.categories.map((category) => category.name).join(', ')}
          </p>
        </li>
      ))}
    </ul>
  );
}

export default Posts;
