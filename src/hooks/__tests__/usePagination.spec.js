import { renderHook, act } from '@testing-library/react-hooks';
import usePagination from '../usePagination';

describe('usePagination hook', () => {
  const testArray = [
    { id: 1, name: 'Item 1' },
    { id: 2, name: 'Item 2' },
    { id: 3, name: 'Item 3' },
    { id: 4, name: 'Item 4' },
    { id: 5, name: 'Item 5' },
    { id: 6, name: 'Item 6' },
  ];

  it('returns the initial array and hasMore flag', () => {
    const { result } = renderHook(() => usePagination(testArray, 3));
    expect(result.current.currentArray).toEqual(testArray.slice(0, 3));
    expect(result.current.hasMore).toBe(true);
  });

  it('loads more items and updates the currentArray and hasMore flag', () => {
    const { result } = renderHook(() => usePagination(testArray, 3));

    act(() => {
      result.current.loadMore();
    });

    expect(result.current.currentArray).toEqual(testArray.slice(0, 6));
    expect(result.current.hasMore).toBe(false);
  });
});
