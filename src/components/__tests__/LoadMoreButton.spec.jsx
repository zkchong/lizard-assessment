import { screen, fireEvent, render } from '@testing-library/react';
import LoadMoreButton from '../LoadMoreButton';

describe('LoadMoreButton component', () => {
  it('should match snapshot', () => {
    expect(render(<LoadMoreButton loadMore={jest.fn} />)).toMatchSnapshot();
  });
});
