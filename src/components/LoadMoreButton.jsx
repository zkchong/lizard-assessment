function LoadMoreButton({ loadMore }) {
  return (
    <button
      className="text-center w-full bg-black hover:bg-gray-700 text-white font-bold py-2 px-4 rounded"
      onClick={loadMore}
    >
      Load More
    </button>
  );
}

export default LoadMoreButton;
